export async function each(
    iterable: Promise<any>[] | string,
    cb: (
        iterable: Promise<any>,
        index?: number,
        iterableLength?: number
    ) => Promise<any>
): Promise<any[]> {
    const result = [];
    for (let i = 0; i < iterable.length; i++) {
        const res = await iterable[i];
        await cb(res, i, iterable.length);
        result.push(res);
    }
    return result;
}
