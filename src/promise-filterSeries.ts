export async function filterSeries(
    iterable: any[] | string,
    cb: (item: any) => Promise<boolean>
): Promise<string | any[]> {
    const results = [];
    for (const item of iterable) {
        if ((await cb(item)) === true) results.push(item);
    }
    return typeof iterable === "string" ? results.join("") : results;
}
