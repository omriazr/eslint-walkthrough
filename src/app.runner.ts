// import log from "@ajar/marker";
// import * as P from "./index.js";
// import Promiseb from "bluebird";

// test delay
// async function printWithDelay(input:string, ms:number){
//   for(const char of input){
//     await P.delay(ms);
//     console.log(char);
//   }
// }

// printWithDelay('omri', 1000);

//test Promise All
// const promise1 = P.echo("1 first resolved value", 3000);
// const promise2 = P.echo("2 second resolved value", 3000);
// const promise3 = P.echo("3 third resolved value", 3000);

// let run = async () => console.log(await P.all([
//     promise1,
//     promise2,
//     promise3
// ]));
// run();

//test props

// let run = async () => console.log(await Promiseb.props({ promise1, promise2, promise3 }));

// run();

//test each

// ( async () => {
//     log.yellow(

//         await P.each('Geronimo', async char => {
//             const c = (await char) as string;
//             log.cyan(`${c} -->`);
//             await P.delay( P.random(2000,500) );
//             log.magenta(`<-- ${c}`);
//             return c.toUpperCase(); // No effect...
//         })
//     )
// })();

// map parallel
// ( async () =>{
//     const promise1 = P.echo("1 first", 1000);
//     const promise2 = P.echo("2 second", 100);
//     const promise3 = P.echo("3 third", 2000);

//     log.yellow(

//         await P.mapParallel([promise1, promise2, promise3] , async p => {
//             let char = await p;
//             log.cyan(`${char} <--`);
//             await P.delay( P.random(2000,500) );
//             log.magenta(`<-- ${char}`);
//             return char.toUpperCase(); // Modify each item in the iterable
//         })

//     )
// })();

/**   P.mapSeries()  **/
// ( async () =>{
//     const promise1 = P.echo("1 first", 1000);
//     const promise2 = P.echo("2 second", 100);
//     const promise3 = P.echo("3 third", 2000);

//     log.yellow(

//         await Promiseb.mapSeries([promise1, promise2, promise3] , async p => {
//             let char = await p;
//             log.cyan(`${char} <--`);
//             await P.delay( P.random(2000,500) );
//             log.magenta(`<-- ${char}`);
//             return char.toUpperCase(); // Modify each item in the iterable
//         })

//     )
// })()

// filterParallel

// (async () =>{
//     log.yellow(
//         // filter only alphabetic characters
//         await P.filterParallel('G<4!e3ro0ni1mo', async char => {
//             log.cyan(`${char} -->`);
//             await P.delay( P.random(2000,500) );
//             log.magenta(`<-- ${char}`);
//             return /^[A-Za-z]+$/.test(char); // test for alphabetic characters
//         })
//     )
// })()

// filter series
// (async () =>{
//     log.yellow(
//         // filter only alphabetic characters
//         await P.filterSeries('G<4!e3ro0ni1mo', async char => {
//             log.cyan(`${char} -->`);
//             await P.delay( P.random(2000,500) );
//             log.magenta(`<-- ${char}`);
//             return /^[A-Za-z]+$/.test(char); // test for alphabetic characters
//         })
//     )
// })()

//reduce
// (async () =>{
//     log.yellow(
//         // filter only alphabetic characters
//         await P.reduce([51,64,25,12,93], async (total,num) => {
//             log.cyan(`${num} -->`);
//             await P.delay( P.random(1000,100) );
//             log.magenta(`<-- ${num}`);
//             return await(total) + num;
//         },0)

//     )
// })()

//race
// (async () => {
//     log.yellow(
//         // filter only alphabetic characters
//         await P.race([
//                 P.echo('first',4000),
//                 P.echo('second',1000),
//                 P.echo('third',3000)
//             ])

//     )
// })();

//some
// ( async () => {
//     log.yellow(
//         // filter only alphabetic characters
//         await P.some([
//                 P.echo('first',4000),
//                 P.echo('second',400),
//                 P.echo('third',3000),
//                 P.echo('forth',3000),
//                 P.echo('fifth',500),
//             ],2)
//     )
// })();
