export function some(iterable: Promise<any>[], num: number) {
    return new Promise(async (resolve) => {
        const arr: any[] = [];
        iterable = await iterable;
        iterable.forEach(async (item) => {
            item.then((item) => {
                if (arr.length < num) {
                    arr.push(item);
                } else resolve(arr);
            });
        });
    });
}
