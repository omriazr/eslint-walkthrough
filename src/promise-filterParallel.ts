export async function filterParallel(
    iterable: any[] | string,
    cb: (item: any) => Promise<boolean>
): Promise<string | any[]> {
    const results = [];
    const pending = Array.from(iterable, (item) => cb(item));

    for (const [i, p] of pending.entries()) {
        if ((await p) === true) results.push(iterable[i]);
    }

    return typeof iterable === "string" ? results.join("") : results;
}
