export async function mapSeries(
    iterable: Promise<any>[],
    cb: (
        iterable: Promise<any>,
        index?: number,
        iterableLength?: number
    ) => Promise<any>
): Promise<any[]> {
    const result = [];
    for (let item of iterable) {
        if (item instanceof Promise) {
            item = await item;
        }
        const itemResult = await cb(item);
        result.push(itemResult);
    }
    return result;
}
