interface promiseObject {
    [key: string]: Promise<any> | any;
}

export async function props(promisesObj: promiseObject) {
    const result: promiseObject = {};
    for (const key in promisesObj) {
        result[key] = await promisesObj[key];
    }
    return result;
}
