export async function race(iterable: Promise<any>[]): Promise<any> {
    return new Promise((resolve, reject) => {
        iterable.forEach((p) => p.then(resolve).catch(reject));
    });
}
