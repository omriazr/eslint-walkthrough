export async function mapParallel(
    iterable: Promise<any>[],
    cb: (
        iterable: Promise<any>,
        index?: number,
        iterableLength?: number
    ) => Promise<any>
) {
    const results = [];
    const pending = Array.from(iterable, (item) => cb(item));

    for (const [i, p] of pending.entries()) {
        if ((await p) === true) results.push(iterable[i]);
    }

    return typeof iterable === "string" ? results.join("") : results;
}
