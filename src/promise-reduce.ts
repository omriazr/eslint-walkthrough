type reduceCb = (
    iterable: Promise<any>,
    index?: number,
    iterableLength?: number
) => Promise<any>;

export async function reduce(
    iterable: (Promise<any> | any)[],
    cb: reduceCb,
    initialValue: any
) {
    let value = (await initialValue) || (await iterable.shift());
    for (let item of iterable) {
        item = await item;
        value = await cb(value, item);
    }
    return value;
}
