export async function all(pendingPromises: Promise<any>[]): Promise<any[]> {
    try {
        const values = [];
        for (const promise of pendingPromises) {
            values.push(await promise);
        }
        return values;
    } catch (err) {
        throw err;
    }
}
