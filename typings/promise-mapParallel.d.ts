export declare function mapParallel(iterable: Promise<any>[], cb: (iterable: Promise<any>, index?: number, iterableLength?: number) => Promise<any>): Promise<string | Promise<any>[]>;
