export declare function mapSeries(iterable: Promise<any>[], cb: (iterable: Promise<any>, index?: number, iterableLength?: number) => Promise<any>): Promise<any[]>;
