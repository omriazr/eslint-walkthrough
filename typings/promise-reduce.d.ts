declare type reduceCb = (iterable: Promise<any>, index?: number, iterableLength?: number) => Promise<any>;
export declare function reduce(iterable: (Promise<any> | any)[], cb: reduceCb, initialValue: any): Promise<any>;
export {};
