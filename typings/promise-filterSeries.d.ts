export declare function filterSeries(iterable: any[] | string, cb: (item: any) => Promise<boolean>): Promise<string | any[]>;
