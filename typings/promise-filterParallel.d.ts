export declare function filterParallel(iterable: any[] | string, cb: (item: any) => Promise<boolean>): Promise<string | any[]>;
