interface promiseObject {
    [key: string]: Promise<any> | any;
}
export declare function props(promisesObj: promiseObject): Promise<promiseObject>;
export {};
