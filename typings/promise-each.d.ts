export declare function each(iterable: Promise<any>[] | string, cb: (iterable: Promise<any>, index?: number, iterableLength?: number) => Promise<any>): Promise<any[]>;
