# Node.js TS 
## @Promises/omriazr

re-implement promises methods:
delay                ( Bluebird     Promise.delay)
all                     ( native         Promise.all  )
props               ( Bluebird     Promise.all  )
each                 ( Bluebird     Promise.each  )
mapParallel     ( Bluebird     Promise.map             -   without the concurrency feature...)  
mapSeries       ( Bluebird     Promise.mapSeries   -  without the concurrency feature...)

filter *               ( Bluebird     Promise.filter ) --> implement twice --> filterParallel and filterSeries
reduce              ( Bluebird     Promise.reduce )
race                   ( native        Promise.race )
some                 ( Bluebird    Promise.some )

